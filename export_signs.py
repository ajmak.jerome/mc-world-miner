"""
Exports previous extracted book texts to a more readable format.
"""

import os.path
import re
import json

from quarry.types.nbt import *

from utils import *

def main(argv):
    # Parse options
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--database", default="world.db", help="database file to write mined data to")
    parser.add_argument("-f", "--format", choices=["text"], default="text", help="the format to use for exporting (currently only text supported)")
    parser.add_argument("--allow-empty", action='store_true', help="if set, empty signs will also be exported")
    parser.add_argument("out_file", help="the file to export the data to")
    args = parser.parse_args(argv)

    db = MCWMDB(args.database)
    with open(args.out_file, "w", encoding="utf-8") as f:
        c = db.cursor()

        # Firstly, we build an association of which content hashes are found where.
        books_found_in = {}

        query = """
        SELECT be_x, be_y, be_z, text1, text2, text3, text4 FROM signs
        """
        count = 0
        for be_x, be_y, be_z, text1, text2, text3, text4 in c.execute(query):
            texts = [text1, text2, text3, text4]
            if all(text == "" for text in texts) and not args.allow_empty:
                continue

            texts = [clean_message(text) for text in texts]
            longest = max(texts, key = len)
            f.write("==============================\n")
            f.write("Sign at x:{}, y:{}, z:{}:\n".format(be_x, be_y, be_z))
            for text in texts:
                leftpad = " " * ((len(longest) - len(text)) // 2)
                f.write("{}{}\n".format(leftpad, text))

            count += 1

        print("Exported {} signs".format(count))


if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
