"""
Exports previous extracted book texts to a more readable format.
"""

import os.path
import re
import json

from quarry.types.nbt import *

from utils import *

def main(argv):
    # Parse options
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--database", default="world.db", help="database file to write mined data to")
    parser.add_argument("-f", "--format", choices=["text"], default="text", help="the format to use for exporting (currently only text supported)")
    parser.add_argument("out_file", help="the file to export the data to")
    args = parser.parse_args(argv)

    db = MCWMDB(args.database)
    with open(args.out_file, "w", encoding="utf-8") as f:
        c = db.cursor()

        # Firstly, we build an association of which content hashes are found where.
        books_found_in = {}

        book_count, page_count = 0, 0

        query = """
        SELECT books.item_id, books.content_hash, books.generation, books.display_name,
               items.mcid, items.count,
               found_in_be.slot, found_in_be.be_x, found_in_be.be_y, found_in_be.be_z,
               found_in_en.slot, found_in_en.en_uuid,
               found_in_inv.slot, found_in_inv.inv_player_uuid, found_in_inv.inv_type,
               block_entities.mcid,
               entities.mcid, entities.x, entities.y, entities.z,
               players.file_uuid
        FROM books

             JOIN items ON books.item_id = items.id
        LEFT JOIN found_in_be ON books.item_id = found_in_be.item_id
        LEFT JOIN found_in_en ON books.item_id = found_in_en.item_id
        LEFT JOIN found_in_inv ON books.item_id = found_in_inv.item_id
        LEFT JOIN block_entities ON found_in_be.be_x = block_entities.x
                                AND found_in_be.be_y = block_entities.y
                                AND found_in_be.be_z = block_entities.z
        LEFT JOIN entities ON found_in_en.en_uuid = entities.uuid
        LEFT JOIN players ON found_in_inv.inv_player_uuid = players.uuid
        """

        for books_item_id, books_content_hash, books_generation, books_display_name, \
        items_mcid, items_count, \
        found_in_be_slot, found_in_be_be_x, found_in_be_be_y, found_in_be_be_z, \
        found_in_en_slot, found_in_en_en_uuid, \
        found_in_inv_slot, found_in_inv_inv_player_uuid, found_in_inv_inv_type, \
        block_entities_mcid, \
        entities_mcid, entities_x, entities_y, entities_z, \
        players_file_uuid in c.execute(query):
            res = []
            if books_content_hash not in books_found_in:
                books_found_in[books_content_hash] = []

            opt_display_name = ""
            if books_display_name is not None:
                opt_display_name = ', display name: {}'.format(books_display_name)

            if found_in_be_be_x is not None:
                res.append("Found {}x {} (item ID {}, generation {}{}) in slot {} of block entity {} at x:{}, y:{}, z:{}\n".format(items_count, items_mcid, books_item_id, books_generation, opt_display_name, found_in_be_slot, block_entities_mcid, found_in_be_be_x, found_in_be_be_y, found_in_be_be_z))
            if found_in_en_en_uuid is not None:
                res.append("Found {}x {} (item ID {}, generation {}{}) in slot {} of entity {} (UUID: {}) at x:{}, y:{}, z:{}\n".format(items_count, items_mcid, books_item_id, books_generation, opt_display_name, found_in_en_slot, entities_mcid, found_in_en_en_uuid, entities_x, entities_y, entities_z))
            if found_in_inv_inv_player_uuid is not None:
                type = "ender chest" if found_in_inv_inv_type == INVENTORY_TYPE_ENDER_CHEST else "inventory"
                res.append("Found {}x {} (item ID {}, generation {}{}) in slot {} of {} of player with UUID {}\n".format(items_count, items_mcid, books_item_id, books_generation, opt_display_name, found_in_inv_slot, type, players_file_uuid))

            books_found_in[books_content_hash] += res
            book_count += 1

        # Secondly, we look at each book's pages
        for content_hash, found_ins in books_found_in.items():
            query = """
            SELECT book_metadata.title, book_metadata.author,
                   book_pages.page_number, book_pages.content
            FROM book_metadata
            LEFT JOIN book_pages ON book_metadata.content_hash = book_pages.content_hash
            WHERE book_metadata.content_hash = ?
            ORDER BY book_pages.page_number
            """
            first = True
            for title, author, page_number, content in c.execute(query, (content_hash, )):
                if first:
                    f.write("=====================================\n")
                    f.write('Book "{}" by {}\n'.format(clean_message(title), author))
                    f.write('(content hash = {})\n'.format(content_hash))
                    for found_in in found_ins:
                        f.write(found_in)
                    f.write("\n")
                    first = False

                f.write("--- Page {} ---\n".format(page_number))
                f.write(clean_message(content) + "\n")
                page_count += 1

        print("Exported {} books and {} total pages".format(book_count, page_count))


if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
