import sqlite3
import json
from quarry.types.chat import Message

INVENTORY_TYPE_MAIN = 0
INVENTORY_TYPE_ENDER_CHEST = 1

class MCWMDB:
    def __init__(self, path):
        self.path = path
        self.conn = sqlite3.connect(path)
        self.migrate()

    def migrate(self):
        c = self.cursor()

        c.execute("CREATE TABLE IF NOT EXISTS block_entities (x INTEGER, y INTEGER, z INTEGER, mcid TEXT, nbt BLOB, PRIMARY KEY (x, y, z))")
        c.execute("CREATE TABLE IF NOT EXISTS entities (uuid TEXT, x INTEGER, y INTEGER, z INTEGER, mcid TEXT, nbt BLOB, PRIMARY KEY (uuid))")
        c.execute("CREATE TABLE IF NOT EXISTS player_inventories (player_uuid TEXT, inventory_type INTEGER, nbt BLOB, PRIMARY KEY (player_uuid, inventory_type))")

        c.execute("CREATE TABLE IF NOT EXISTS players (uuid TEXT PRIMARY KEY, file_uuid TEXT, display_name TEXT)")

        c.execute("CREATE TABLE IF NOT EXISTS items (id INTEGER PRIMARY KEY, mcid TEXT, count INTEGER, nbt BLOB)")

        c.execute("CREATE TABLE IF NOT EXISTS found_in_en (item_id INTEGER PRIMARY KEY, slot INTEGER, en_uuid TEXT)")
        c.execute("CREATE TABLE IF NOT EXISTS found_in_be (item_id INTEGER PRIMARY KEY, slot INTEGER, be_x INTEGER, be_y INTEGER, be_z INTEGER)")
        c.execute("CREATE TABLE IF NOT EXISTS found_in_inv (item_id INTEGER PRIMARY KEY, slot INTEGER, inv_player_uuid INTEGER, inv_type INTEGER)")

        c.execute("CREATE TABLE IF NOT EXISTS books (item_id INTEGER PRIMARY KEY, content_hash INTEGER, generation INTEGER, display_name TEXT)")
        c.execute("CREATE TABLE IF NOT EXISTS book_metadata (content_hash INTEGER PRIMARY KEY, title TEXT, author TEXT)")
        c.execute("CREATE TABLE IF NOT EXISTS book_pages (content_hash INTEGER, page_number INTEGER, content TEXT, PRIMARY KEY (content_hash, page_number))")

        c.execute("CREATE TABLE IF NOT EXISTS signs (be_x INTEGER, be_y INTEGER, be_z INTEGER, text1 TEXT, text2 TEXT, text3 TEXT, text4 TEXT, PRIMARY KEY (be_x, be_y, be_z))")

        self.commit()

    def cursor(self):
        return self.conn.cursor()

    def commit(self):
        self.conn.commit()

    def postproc(self, tables):
        print("Post-processing database...")

        c = self.cursor()
        for table in tables:
            c.execute("UPDATE " + table + " SET nbt = cast(nbt as BLOB)")
        self.commit()

        print("Done")

    def close(self):
        self.conn.close()

def uuid_combine_most_least(most, least):
    most = (most + 2**64) & (2**64 - 1)
    least = (least + 2**64) & (2**64 - 1)
    return most << 64 | least

def uuid_combine_4(array):
    return uuid_combine_most_least((array[0] << 32) | array[1], (array[2] << 32) | array[3])

def hex_uuid(int_uuid):
    total = int_uuid & (2**128 - 1)
    str1 = hex(total)[2:].zfill(32)
    return "{}-{}-{}-{}-{}".format(str1[0:8], str1[8:12], str1[12:16], str1[16:20], str1[20:32])

def get_uuid(tag):
    if "UUID" in tag and not "UUIDMost" in tag:
        return hex_uuid(uuid_combine_4(tag["UUID"].value))
    elif "UUIDMost" in tag and not "UUID" in tag:
        return hex_uuid(uuid_combine_most_least(tag["UUIDMost"].value, tag["UUIDLeast"].value))
    else:
        print("UUID ambiguous! Using new format UUID")
        return hex_uuid(uuid_combine_4(tag["UUID"].value))

def clean_message(what):
    if what is None:
        return "(None)"
    if what.startswith('{"'):
        what = json.loads(what)
    return Message(what).to_string()
