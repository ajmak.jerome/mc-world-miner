"""
Loads entities and block entities from a region file and inserts the result into
an SQLite database.
"""

import os
import re
import traceback

from quarry.types.nbt import *
from quarry.types.chunk import BlockArray

from utils import *

def main(argv):
    # Parse options
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--database", default="world.db", help="database file to write mined data to")
    parser.add_argument("--cx", type=int, default=None, help="if specified, only chunks with this X coordinate will be considered")
    parser.add_argument("--cz", type=int, default=None, help="if specified, only chunks with this Z coordinate will be considered")
    parser.add_argument("path", nargs='+', help="paths of region files to load. If a single directory is specified, it will be treated as a region folder and all .mca files in it will be loaded")
    args = parser.parse_args(argv)

    db = MCWMDB(args.database)

    if len(args.path) == 1 and os.path.isdir(args.path[0]):
        region_files = [os.path.join(args.path[0], filename) for filename in os.listdir(args.path[0]) if filename.endswith('.mca')]
    else:
        region_files = args.path

    count = 0
    for path in region_files:
        try:
            basename = os.path.basename(path)
            match = re.match(r"r\.(-?\d+)\.(-?\d+)\.mca", basename)
            if match is not None:
                rx, rz = int(match.group(1)), int(match.group(2))
            else:
                rx, rz = None, None

            total_be, total_en, total_chunk = 0, 0, 0

            with RegionFile(path) as region:
                for cx in range(0, 32):
                    for cz in range(0, 32):
                        if (args.cx is not None and cx != args.cx) or (args.cz is not None and cz != args.cz):
                            continue

                        try:
                            chunk = region.load_chunk(cx, cz)
                        except ValueError:
                            # Chunk does not exist
                            continue

                        level = chunk.body.value["Level"].value
                        total_chunk += 1

                        c = db.cursor()

                        if 'Entities' in level:
                            for en_raw in level["Entities"].value:
                                total_en += 1
                                en = en_raw.value
                                x, y, z = map(lambda coord: coord.value, en["Pos"].value)
                                mcid = en["id"].value
                                uuid = get_uuid(en)
                                nbt = en_raw.to_bytes()
                                c.execute("INSERT OR REPLACE INTO entities (x, y, z, mcid, uuid, nbt) VALUES (?, ?, ?, ?, ?, ?)", (x, y, z, mcid, uuid, nbt))

                        if 'TileEntities' in level:
                            for be_raw in level["TileEntities"].value:
                                total_be += 1
                                be = be_raw.value
                                x, y, z = be["x"].value, be["y"].value, be["z"].value
                                mcid = be["id"].value
                                nbt = be_raw.to_bytes()
                                c.execute("INSERT OR REPLACE INTO block_entities (x, y, z, mcid, nbt) VALUES (?, ?, ?, ?, ?)", (x, y, z, mcid, nbt))

            print("Processed {} total entities and {} total block entities in {} chunks for region rx:{}, rz:{}".format(total_en, total_be, total_chunk, rx, rz))
            db.commit()
            count += 1
        except Exception:
            traceback.print_exc()
            print("For above exception: occurred while processing file {}".format(path))

    db.postproc(("entities", "block_entities"))
    print("Mined {} region files".format(count))

if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
