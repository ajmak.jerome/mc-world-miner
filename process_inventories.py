"""
Processes inventories stored in an SQLite database, as created by one or more of
the mine_xyz.py scripts.
"""

import os.path
import re
import traceback

from quarry.types.nbt import *

from utils import *

def possibly_insert_item(c, item, all_items = False):
    tag = None

    if 'tag' in item:
        tag = item['tag']
    else:
        if not all_items:
            return False

    mcid = item['id'].value
    count = item['Count'].value
    c.execute("INSERT INTO items (mcid, count, nbt) VALUES (?, ?, ?)", (mcid, count, tag.to_bytes()))
    return True

# Entities
def en_insert(c, item, uuid, args):
    if not possibly_insert_item(c, item, args.all_items):
        return

    if 'Slot' in item:
        c.execute("INSERT INTO found_in_en (item_id, slot, en_uuid) VALUES (?, ?, ?)", (c.lastrowid, item['Slot'].value, uuid))
    else:
        c.execute("INSERT INTO found_in_en (item_id, en_uuid) VALUES (?, ?)", (c.lastrowid, uuid))

def en_insert_multi(c, items, uuid, args):
    for item_tag in items:
        item = item_tag.value
        en_insert(c, item, uuid, args)

def process_entity_generic_multi(c, en, uuid, args):
    if 'Items' in en:
        en_insert_multi(c, en['Items'].value, uuid, args)

def process_entity_generic_single(c, en, uuid, args):
    if 'Item' not in en:
        return
    en_insert(c, en['Item'].value, uuid, args)

def process_entity_mob(c, en, uuid, args):
    items = []
    if 'HandItems' in en:
        items += en['HandItems'].value
    if 'ArmorItems' in en:
        items += en['ArmorItems'].value
    en_insert_multi(c, items, uuid, args)

ENTITY_MAPPING = {
    'minecraft:donkey': process_entity_generic_multi,
    'minecraft:horse': process_entity_generic_multi,
    'minecraft:mule': process_entity_generic_multi,
    'minecraft:llama': process_entity_generic_multi,
    'minecraft:trader_llama': process_entity_generic_multi,
    'minecraft:item': process_entity_generic_single,
    'minecraft:chest_minecart': process_entity_generic_multi,
    'minecraft:hopper_minecart': process_entity_generic_multi,
    'minecraft:item_frame': process_entity_generic_single
}

def process_entities(db, args):
    c = db.cursor()
    result = c.execute("SELECT mcid, uuid, nbt FROM entities").fetchall()
    for mcid, uuid, nbt in result:
        try:
            en = TagCompound.from_bytes(nbt).value
            if mcid in ENTITY_MAPPING:
                process_func = ENTITY_MAPPING[mcid]
                process_func(c, en, uuid, args)
            process_entity_mob(c, en, uuid, args)
        except Exception:
            traceback.print_exc()
            print("For above exception: occurred while processing entity {} (UUID: {})".format(mcid, uuid))

    print("Processed items from entities")
    db.commit()

# Block entities
def be_insert(c, item, x, y, z, args):
    if not possibly_insert_item(c, item, args.all_items):
        return

    if 'Slot' in item:
        c.execute("INSERT INTO found_in_be (item_id, slot, be_x, be_y, be_z) VALUES (?, ?, ?, ?, ?)", (c.lastrowid, item['Slot'].value, x, y, z))
    else:
        c.execute("INSERT INTO found_in_be (item_id, be_x, be_y, be_z) VALUES (?, ?, ?, ?)", (c.lastrowid, x, y, z))

def process_block_entity_generic(c, be, x, y, z, args):
    if 'Items' in be:
        items = be['Items'].value
        for item_tag in items:
            item = item_tag.value
            be_insert(c, item, x, y, z, args)

def process_block_entity_jukebox(c, be, x, y, z, args):
    if 'RecordItem' not in be:
        return
    be_insert(c, be['RecordItem'].value, x, y, z, args)

def process_block_entity_lectern(c, be, x, y, z, args):
    if 'Book' not in be:
        return
    be_insert(c, be['Book'].value, x, y, z, args)

BLOCK_ENTITY_MAPPING = {
    'minecraft:chest': process_block_entity_generic,
    'minecraft:furnace': process_block_entity_generic,
    'minecraft:blast_furnace': process_block_entity_generic,
    'minecraft:smoker': process_block_entity_generic,
    'minecraft:campfire': process_block_entity_generic,
    'minecraft:soul_campfire': process_block_entity_generic,
    'minecraft:cauldron': process_block_entity_generic,
    'minecraft:dispenser': process_block_entity_generic,
    'minecraft:dropper': process_block_entity_generic,
    'minecraft:hopper': process_block_entity_generic,
    'minecraft:shulker_box': process_block_entity_generic,
    'minecraft:barrel': process_block_entity_generic,
    'minecraft:jukebox': process_block_entity_jukebox,
    'minecraft:lectern': process_block_entity_lectern,
}

def process_block_entities(db, args):
    c = db.cursor()
    desired_mcids = list(BLOCK_ENTITY_MAPPING.keys())
    query = "SELECT mcid, x, y, z, nbt FROM block_entities WHERE mcid IN ({})".format(','.join('?' * len(desired_mcids)))
    result = c.execute(query, tuple(desired_mcids)).fetchall()
    for mcid, x, y, z, nbt in result:
        try:
            if mcid not in BLOCK_ENTITY_MAPPING:
                print("Invalid MCID found: {}".format(mcid))
                continue

            be = TagCompound.from_bytes(nbt).value
            process_func = BLOCK_ENTITY_MAPPING[mcid]
            process_func(c, be, x, y, z, args)
        except Exception:
            traceback.print_exc()
            print("For above exception: occurred while processing block entity {} at x:{}, y:{}, z:{}".format(mcid, x, y, z))

    print("Processed items from block entities")
    db.commit()

# Player inventories
def process_player_inventories(db, args):
    c = db.cursor()
    result = c.execute("SELECT player_uuid, inventory_type, nbt FROM player_inventories").fetchall()
    for player_uuid, inventory_type, nbt in result:
        try:
            inventory = TagList.from_bytes(nbt).value
            for item_tag in inventory:
                item = item_tag.value
                if possibly_insert_item(c, item, args.all_items):
                    c.execute("INSERT INTO found_in_inv (item_id, slot, inv_player_uuid, inv_type) VALUES (?, ?, ?, ?)", (c.lastrowid, item['Slot'].value, player_uuid, inventory_type))
        except Exception:
            traceback.print_exc()
            print("For above exception: occurred while processing {} of player with UUID {}".format("ender chest" if inventory_type == INVENTORY_TYPE_ENDER_CHEST else "inventory", player_uuid))

    print("Processed items from player inventories")
    db.commit()

def main(argv):
    # Parse options
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--database", default="world.db", help="database file to write mined data to")
    parser.add_argument("--no-entities", action='store_true', help="if set, entities will not be processed")
    parser.add_argument("--no-block-entities", action='store_true', help="if set, block entities will not be processed")
    parser.add_argument("--no-player-inventories", action='store_true', help="if set, player inventories will not be processed")
    parser.add_argument("--all-items", action='store_true', help="if set, items without an NBT tag will be extracted from inventories (in addition to the ones with a tag)")
    args = parser.parse_args(argv)

    db = MCWMDB(args.database)

    if not args.no_entities:
        process_entities(db, args)

    if not args.no_block_entities:
        process_block_entities(db, args)

    if not args.no_player_inventories:
        process_player_inventories(db, args)

    db.postproc(("items", ))


if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
