"""
Loads information from a level.dat file and inserts it into an SQLite database.
"""

import os.path
import re

from quarry.types.nbt import *

from utils import *
from mine_player_data import process_player

def main(argv):
    # Parse options
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--database", default="world.db", help="database file to write mined data to")
    parser.add_argument("path", help="path of level.dat file to load")
    args = parser.parse_args(argv)

    db = MCWMDB(args.database)

    file = NBTFile.load(args.path)
    level = file.root_tag.body.value["Data"].value

    if "Player" in level:
        player = level["Player"]
        process_player(db.cursor(), player)

        db.commit()
        db.postproc(("player_inventories", ))
        print("Processed player data in level file")
    else:
        print("No player data present in file. Nothing to do")

if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
