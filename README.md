# Minecraft World Miner

This is a more or less loose set of scripts to help in data-mining information
from Minecraft worlds. Currently, the following features are supported:

- **mine_region_file.py**: extract entities and block entities from region files
- **mine_player_data.py** and **mine_level_data.py**: extract player inventories
  and ender chests from `<player>.dat`/`level.dat` files
- **process_inventories.py**: extract items from inventories found using the
  above means
- **extract_books.py** and **export_books.py**: extract book metadata and
  content from items, and export to a text file
- **extract_signs.py** and **export_signs.py**: extract sign contents from mined
  block entities, and export to a text file

The scripts work by storing information in a SQLite database, which can then be
read again by later scripts. This has several advantages, one of which is that
it is relatively simple to implement further custom data-mining methods. The
database file to store information in is specified by the `--database` argument
available for each script; if none is specified, a file called `world.db` in the
current directory is used.

The scripts should work well even for very large worlds. (TODO: elaborate here)

# Installation and usage

You will first need:

- Python 3. I have tested version 3.5 and above, but some lower versions might
  also work. Don't use Python 2; while some of the scripts work in it, not all
  of them do, and it won't be obvious that this is the reason why.
- The [Quarry](https://pypi.org/project/quarry/) library. If you have `pip`
  available, installing this is as simple as `pip install quarry`.

Note that I have only tested the scripts on Linux. There is no reason they would
not also work on Windows; if you have tested them successfully, feel free to
report back.

Then, simply clone this repository into a folder from which you will execute
the scripts.

An exemplary workflow could look like this:

```sh
python mine_region_file.py /path/to/world/region
python mine_player_data.py /path/to/world/playerdata
python process_inventories.py
python extract_books.py
python export_books.py books.txt
python extract_signs.py
python export_signs.py signs.txt
```

This will export all the books and signs from the world and player inventories
to `books.txt` and `signs.txt`, respectively.
