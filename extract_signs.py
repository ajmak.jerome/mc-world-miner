"""
Extracts sign texts from block entities mined by mine_region_file.py.
"""

import os.path
import re

from quarry.types.nbt import *

from utils import *

def main(argv):
    # Parse options
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--database", default="world.db", help="database file to read/write data from/to")
    args = parser.parse_args(argv)

    db = MCWMDB(args.database)

    c = db.cursor()
    result = c.execute("SELECT x, y, z, nbt FROM block_entities WHERE mcid = ?", ("minecraft:sign", )).fetchall()

    count = 0

    for x, y, z, nbt in result:
        sign = TagCompound.from_bytes(nbt).value

        texts = [(sign['Text' + str(n)].value if ('Text' + str(n)) in sign else "") for n in range(1, 5)]
        c.execute("INSERT OR REPLACE INTO signs (be_x, be_y, be_z, text1, text2, text3, text4) VALUES (?, ?, ?, ?, ?, ?, ?)", (x, y, z) + tuple(texts))
        count += 1

    db.commit()
    print("Extracted {} signs".format(count))

if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
